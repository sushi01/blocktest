﻿using UnityEngine;
using System.Collections;

public class ball : MonoBehaviour {

    // Use this for initialization
    public float speed = 8;

    void Start() {
        Rigidbody r = GetComponent<Rigidbody>();
        r.velocity = new Vector3(-1, 1, 0).normalized * speed;

    }

    // Update is called once per frame
    void Update() {

    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Block")
        {
            Destroy(collision.gameObject);

        }
    
 

    }

}
